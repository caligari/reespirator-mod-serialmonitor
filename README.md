# Mod SerialMonitor

Reespirator module implementing the serial protocol to external machine monitor.

## Dependencies

- [LoopTicker.hpp][1]
- [reespirator/Core][2]

[1]: https://gitlab.com/reespirator/firmware/lib-loopticker
[2]: https://gitlab.com/reespirator/firmware/core-2020
