/** HmiManager.cpp
 *
 * HmiManager class.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#include "HmiManager.hpp"

namespace reespirator {


HmiManager::HmiManager(DRE& dre) :
    _receiveFsm(dre)
{
}


void HmiManager::update(u32 now_millis)
{
    _receiveFsm.run();
}

}; // namespace reespirator