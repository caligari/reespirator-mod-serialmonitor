/** HmiManager.hpp
 *
 * HmiManager class.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _HMI_MANAGER_H
#define _HMI_MANAGER_H

#include <LoopTicker.hpp>
#include "Hmi01Fsm.hpp"

// minimum time in milliseconds to wait after sending a frame
#define MS_GAP_AFTER_FRAME 2

namespace reespirator {

/**
 * @brief Human-Machine Interface manager for Reespirator serial monitor protocol.
 * @see Protocol specification: https://bit.ly/reespirator-monitor-protocol
 * 
 * It does just one subtask for each ticker update call. Subtasks are:
 * - Receive and process incoming frame from remote host (it includes sending 
 *   some response related to the incoming frame, when needed).
 * - Detect changes in DRI container to send alarm events and new cycle data 
 *   to remote host.
 * 
 * Outgoing frames are composed in this manager but copied to the SerialPort 
 * buffer. No window buffers here but generated frames can be composed
 * again with fresh data in case of remote host doesn't ACK frames.
 *  
 */
class HmiManager
{
    public:

        /**
         * @brief Construct a new Hmi Manager object.
         * 
         * @param dre Reference to the Reespirator DRE container.
         */
        HmiManager(DRE& dre);

        /**
         * @brief Task entry-point to process incoming frames and send outgoing frames.
         * 
         * @param now_millis 
         */
        void update(u32 now_millis);

        /**
         * @brief Static class method to allow the deference of the object instance.
         * 
         * @param object_ptr Pointer to the HmiManager instance.
         * @param loop_ticker Pointer to the LoopTicker helper calling this method.
         */
        static void tickUpdate(const void* object_ptr, LoopTicker* loop_ticker)
        {
            ((HmiManager*) object_ptr)->update(loop_ticker->getLoopMs32());
        }

    private:

        Hmi01Fsm _receiveFsm;
};

#endif // _HMI_MANAGER_H

}; // namespace reespirator